package presentationLayer;


import java.io.IOException;

import businessObjects.ITrainer;
import dataLayer.dataAccessObjects.ITrainerDao;
import exceptions.NoDatabaseFoundException;
import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;
import exceptions.NoTrainerFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainWindowController {
	@FXML
	private TextArea currentTrainerOutput;
	
	@FXML
	private TableView<ITrainer> trainerTable;

	@FXML
	private TextField nameID;
	@FXML
	private TextField ageID;
	@FXML
	private TextField xpID;
	
	private ITrainerDao dao;
	private Stage nextStage;
	private ITrainer currentTrainer;
	private boolean createSomething;
	
	public MainWindowController(ITrainerDao dao) {
		this.dao = dao;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateWindow.fxml"));
			loader.setController(this);
			Parent pane = loader.load();
			Scene scene = new Scene(pane);
			//MainWindowController mainWindowController = loader.getController();
			nextStage = new Stage();
			nextStage.setTitle("New Trainer");
			nextStage.setScene(scene);
			nextStage.initStyle(StageStyle.DECORATED);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	private void initColumn(TableColumn<ITrainer, String> col, double width, boolean resizeable) {
		col.setMaxWidth(width);
		col.setMinWidth(width);
		col.setPrefWidth(width);
		col.setResizable(false);
	}
	
	public void init() {
		trainerTable.getColumns().clear();
		
		TableColumn<ITrainer, String> col1 = new TableColumn<ITrainer, String>("Id");
		TableColumn<ITrainer, String> col2 = new TableColumn<ITrainer, String>("Name");
		TableColumn<ITrainer, String> col3 = new TableColumn<ITrainer, String>("Age");
		TableColumn<ITrainer, String> col4 = new TableColumn<ITrainer, String>("Experience");
		
		col1.setCellValueFactory(new PropertyValueFactory<ITrainer, String>("Id"));
		col2.setCellValueFactory(new PropertyValueFactory<ITrainer, String>("Name"));
		col3.setCellValueFactory(new PropertyValueFactory<ITrainer, String>("Age"));
		col4.setCellValueFactory(new PropertyValueFactory<ITrainer, String>("Experience"));
		
		initColumn(col1, 30, false);
		initColumn(col2, 157, false);
		initColumn(col3, 40, false);
		initColumn(col4, 75, false);
		
		trainerTable.getColumns().add(col1);
		trainerTable.getColumns().add(col2);
		trainerTable.getColumns().add(col3);
		trainerTable.getColumns().add(col4);
	}
	
	private void createDialogWindow() {
		nameID.setText("");
		ageID.setText("");
		xpID.setText("");
		nextStage.showAndWait();
	}
	
	@FXML
	protected void clickCreate(ActionEvent event) {
		createSomething = true;
		createDialogWindow();
	}
	
	@FXML 
	protected void clickDelete(ActionEvent event) {
	//was wenn kein Trainer da ist bei save auch
		try {
			dao.delete(currentTrainer);
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}
	}
	
	@FXML
	protected void clickEdit(ActionEvent event) {
		createSomething = false;
		createDialogWindow();
	}
	
	@FXML
	protected void clickSearch(ActionEvent event) {
		try {
			TextInputDialog uiDialog = new TextInputDialog();
			uiDialog.setTitle("Search");
			uiDialog.setHeaderText("Search something:");
			uiDialog.setContentText("Search: ");
			uiDialog.showAndWait();
			currentTrainer = dao.select(Integer.parseInt(uiDialog.getResult()));
			currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}
	}
	
	@FXML
	protected void clickSave(ActionEvent event) {
		try {
			dao.save(currentTrainer);
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}
		clickPrintAll(event);
	}
	
	@FXML
	protected void clickPrintAll(ActionEvent event) {
		System.out.println(trainerTable);
		trainerTable.getItems().clear();
		try {
			trainerTable.getItems().addAll(dao.select());
			
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}
	}
	
	@FXML
	protected void clickPrev(ActionEvent event) {
		try {
			currentTrainer = dao.previous(currentTrainer);
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoPreviousTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}
		currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
	}
	
	@FXML
	protected void clickNext(ActionEvent event) {
		try {
			currentTrainer = dao.next(currentTrainer);
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoNextTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch (NoTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}
		currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
	}
	
	@FXML
	protected void clickFirstDatasort(ActionEvent event) {
			try {
				currentTrainer = dao.first();
			} catch (NoDatabaseFoundException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(e.getMessage());
				alert.show();
			} catch (NoTrainerFoundException e) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setContentText(e.getMessage());
				alert.show();
			}
			currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
	}
	
	@FXML
	protected void clickLastDatasort(ActionEvent event) {
		try {
			currentTrainer = dao.last();
		}catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}catch(NoTrainerFoundException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setContentText(e.getMessage());
			alert.show();
		}
		currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
	}
	
	@FXML
	protected void clickOk(ActionEvent event) {
		try {
			if(createSomething) {
				currentTrainer = dao.create();
			}
			currentTrainer.setName(nameID.getText());			
			currentTrainer.setAge(Integer.parseInt(ageID.getText()));			
			currentTrainer.setExperience(Integer.parseInt(xpID.getText()));			
			currentTrainerOutput.setText("Your Trainer: " + currentTrainer);
			
		} catch (NoDatabaseFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e.getMessage());
			alert.show();
		}
			nextStage.close();
	}
	
	@FXML
	protected void clickCancel(ActionEvent event) {
		nextStage.close();
	}
}
