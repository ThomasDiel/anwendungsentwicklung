package presentationLayer;

import java.io.IOException;

import dataLayer.DataLayerManager;
import dataLayer.dataAccessObjects.ITrainerDao;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ThisJavaFX extends Application{
	MainWindowController mainWindowController;
	ITrainerDao dao;
	
	public ThisJavaFX() {
		dao = DataLayerManager.getInstance().getDataLayer().getTrainerDao();
		mainWindowController = new MainWindowController(dao);
	}
	@Override
	public void start(Stage primaryStage){
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
			loader.setController(mainWindowController);
			Parent pane = loader.load();
			mainWindowController.init();
			Scene scene = new Scene(pane);
			primaryStage.setTitle("Trainer DAO");
			primaryStage.setScene(scene);
			primaryStage.show();
					
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
