package exceptions;

public class NoPreviousTrainerFoundException extends Exception{

	private static final long serialVersionUID = -8091066412939492064L;
private String exceptionMessage;
	
	public NoPreviousTrainerFoundException(String message) {
		this.exceptionMessage = message;
	}
	
	@Override
	public String getMessage() {
		return exceptionMessage;
	}

}
