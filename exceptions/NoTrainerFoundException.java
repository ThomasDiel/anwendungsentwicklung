package exceptions;

public class NoTrainerFoundException extends Exception{
	
	private static final long serialVersionUID = 8872220253066238391L;
	private String exceptionMessage;
	
	public NoTrainerFoundException(String message) {
		this.exceptionMessage = message;
	}
	
	@Override
	public String getMessage() {
		return exceptionMessage;
	}

}
