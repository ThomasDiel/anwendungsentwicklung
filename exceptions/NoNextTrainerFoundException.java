package exceptions;

public class NoNextTrainerFoundException extends Exception{


	private static final long serialVersionUID = 3661776866884315245L;
	private String exceptionMessage;
	
	public NoNextTrainerFoundException(String message) {
		this.exceptionMessage = message;
	}
	
	@Override
	public String getMessage() {
		return exceptionMessage;
	}
	
	
	
	

}
