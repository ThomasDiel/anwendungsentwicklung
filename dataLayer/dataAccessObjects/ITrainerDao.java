package dataLayer.dataAccessObjects;

import java.util.List;

import businessObjects.ITrainer;
import exceptions.NoDatabaseFoundException;
import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;
import exceptions.NoTrainerFoundException;

/**
 * 
 * @author Ricky Rusli           
 *
 */
public interface ITrainerDao {
	
	/**
	 * Creates a ITrainer object filled with initial values. The object does not
	 * have an database entry.
	 * @return ITrainer object containing initial values
	 * @throws NoDatabaseFoundException 
	 */
	public ITrainer create() throws NoDatabaseFoundException;

	/**
	 * The database entry from the ITrainer object.
	 * @param trainer ITrainer object to be deleted in the database
	 * @throws NoTrainerFoundException 
	 */
	public void delete(ITrainer trainer) throws NoDatabaseFoundException, NoTrainerFoundException;

	/**
	 * Returns the first entry in the database.
	 * @return first entry of the database as ITrainer object
	 */
	public ITrainer first() throws NoDatabaseFoundException, NoTrainerFoundException;

	/**
	 * Returns the last entry in the database.
	 * @return first last of the database as ITrainer object
	 */
	public ITrainer last() throws NoDatabaseFoundException, NoTrainerFoundException;

	/**
	 * Returns the next entry in the database.
	 * @return next entry of the database as ITrainer object
	 */
	public ITrainer next(ITrainer trainer) throws NoDatabaseFoundException, NoNextTrainerFoundException, NoTrainerFoundException;

	/**
	 * Returns the previous entry in the database.
	 * @return previous entry of the database as ITrainer object
	 * @throws NoPreviousTrainerFoundException 
	 * @throws NoTrainerFoundException 
	 */
	public ITrainer previous(ITrainer trainer) throws NoDatabaseFoundException, NoPreviousTrainerFoundException, NoTrainerFoundException;

	/**
	 * Writes a ITrainer object into the database.
	 * @param trainer object informations to be written
	 */
	public void save(ITrainer trainer) throws NoDatabaseFoundException;

	/**
	 * Reads all entries from the database and returns them as a List of ITrainer
	 * @return List of ITrainer of all entries
	 */
	public List<ITrainer> select() throws NoDatabaseFoundException;

	/**
	 * Reads one specific entry from the database and returns it as a ITrainer object
	 * @param id id of the database entry
	 * @return ITrainer object of the entry
	 */
	public ITrainer select(int id) throws NoDatabaseFoundException, NoTrainerFoundException;
}
