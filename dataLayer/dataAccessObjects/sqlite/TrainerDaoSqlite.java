package dataLayer.dataAccessObjects.sqlite;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import businessObjects.ITrainer;
import dataLayer.businessObjects.Trainer;
import dataLayer.dataAccessObjects.ITrainerDao;
import exceptions.NoDatabaseFoundException;
import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;
import exceptions.NoTrainerFoundException;

public class TrainerDaoSqlite implements ITrainerDao {

	// SQL command to select all
	private final String SELECTALL = "SELECT * FROM ";
	// Table name
	private final String CLASSNAME = "Trainer ";
	// Connection path of the table
	private final String CONNECTIONSTRING = "jdbc:sqlite:C:\\Users\\admin_lop\\eclipse-workspace\\frxter\\Trainer.db3";

	private final String NOTRAINERFOUND = "No Trainer found exception!";
	private Connection conn = null;

	@Override
	public ITrainer create() throws NoDatabaseFoundException {

		String sql = "SELECT MAX(id) FROM Trainer";
		Trainer trainer = new Trainer();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.getInt("MAX(id)") < 1) {
				trainer.setId(1);
			} else {
				trainer.setId(rs.getInt("MAX(id)") + 1);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return trainer;
	}

	@Override
	public void delete(ITrainer trainer) throws NoDatabaseFoundException, NoTrainerFoundException {

		String sql = "DELETE FROM " + CLASSNAME + " WHERE id = ?";
		List<ITrainer> temp1 = select();
		List<ITrainer> temp2 = null;

		try {
			PreparedStatement psmt = connection().prepareStatement(sql);

			psmt.setInt(1, trainer.getId());
			psmt.executeUpdate();
			psmt.close();

			temp2 = select();

			if (temp2.size() >= temp1.size()) {
				throw new NoTrainerFoundException(NOTRAINERFOUND);
			}

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public ITrainer first() throws NoDatabaseFoundException, NoTrainerFoundException {

		String sql = SELECTALL + CLASSNAME.trim() + ";";
		Trainer trainer = new Trainer();
		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			convertSqlToTrainer(rs, trainer);

			closer(stmt, rs);
			if (trainer.getId() < 1) {
				throw new NoTrainerFoundException(NOTRAINERFOUND);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return trainer;
	}

	@Override
	public ITrainer last() throws NoDatabaseFoundException, NoTrainerFoundException {

		String sql = SELECTALL + CLASSNAME.trim() + ";";
		Trainer trainer = new Trainer();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next())
				;

			convertSqlToTrainer(rs, trainer);
			if (trainer.getId() < 1) {
				throw new NoTrainerFoundException(NOTRAINERFOUND);
			}

			closer(stmt, rs);
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getMessage() + e.getCause());
		}

		return trainer;
	}

	@Override
	public ITrainer next(ITrainer trainer)
			throws NoDatabaseFoundException, NoNextTrainerFoundException, NoTrainerFoundException {

		String sql = SELECTALL + CLASSNAME;
		Trainer resultTrainer = new Trainer();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (trainer.getId() == rs.getInt("id")) {
					if (rs.next()) {
						convertSqlToTrainer(rs, resultTrainer);
					} else {
						closer(stmt, rs);
						throw new NoNextTrainerFoundException("No next Trainer found!");
					}
					break;
				}
			}

			if (resultTrainer.getId() < 1) {
				closer(stmt, rs);
				throw new NoTrainerFoundException(NOTRAINERFOUND);
			}

			closer(stmt, rs);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return resultTrainer;
	}

	@Override
	public ITrainer previous(ITrainer trainer)
			throws NoDatabaseFoundException, NoPreviousTrainerFoundException, NoTrainerFoundException {

		String sql = SELECTALL + CLASSNAME;
		Trainer resultTrainer = new Trainer();
		Trainer prev = new Trainer();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				convertSqlToTrainer(rs, prev);
				if (trainer.getId() == rs.getInt("id")) {
					if (prev.getId() >= 1) {
						resultTrainer = copyTrainer(prev);
					} else {
						closer(stmt, rs);
						throw new NoPreviousTrainerFoundException("No previous Trainer found!");
					}
					break;
				}
			}

			if (resultTrainer.getId() < 1) {
				closer(stmt, rs);
				throw new NoTrainerFoundException(NOTRAINERFOUND);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return resultTrainer;
	}

	@Override
	public void save(ITrainer trainer) throws NoDatabaseFoundException {

		String selectSQL = "SELECT * FROM Trainer WHERE id = " + trainer.getId() + ";";
		String insertSQL = "INSERT INTO Trainer (id, name, age, experience) " + "VALUES (?,?,?,?)";
		String updateSQL = "UPDATE Trainer SET name = ?, age = ?, experience = ? " + "WHERE id = " + trainer.getId()
				+ "";

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(selectSQL);
			PreparedStatement pstmt = null;

			if (!rs.isClosed()) {
				pstmt = conn.prepareStatement(updateSQL);

				pstmt.setString(1, trainer.getName());
				pstmt.setInt(2, trainer.getAge());
				pstmt.setInt(3, trainer.getExperience());

				pstmt.executeUpdate();
			} else {
				pstmt = conn.prepareStatement(insertSQL);

				pstmt.setInt(1, trainer.getId());
				pstmt.setString(2, trainer.getName());
				pstmt.setInt(3, trainer.getAge());
				pstmt.setInt(4, trainer.getExperience());

				pstmt.executeUpdate();
				System.out.println("here");
			}
			System.out.println(insertSQL);
			closer(stmt, rs, pstmt);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public List<ITrainer> select() throws NoDatabaseFoundException {
		String sql = SELECTALL + CLASSNAME + ";";
		List<ITrainer> trainerList = new ArrayList<ITrainer>();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Trainer trainer = new Trainer();
				convertSqlToTrainer(rs, trainer);
				trainerList.add(trainer);
			}

			closer(stmt, rs);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return trainerList;
	}

	@Override
	public ITrainer select(int id) throws NoDatabaseFoundException, NoTrainerFoundException {

		String sql = "SELECT * FROM Trainer WHERE id=" + id + ";";
		Trainer trainer = new Trainer();

		try {
			Statement stmt = connection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			trainer.setId(rs.getInt("id"));
			trainer.setAge(rs.getInt("age"));
			trainer.setName(rs.getString("name"));
			trainer.setExperience(rs.getInt("experience"));
			closer(stmt, rs);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return trainer;
	}

	/**
	 * Make a connection to the table with the Connectionpath
	 * 
	 * @return Connectionobject for SQL database
	 * @throws NoDatabaseFoundException
	 */
	private Connection connection() throws NoDatabaseFoundException {
		if (conn == null) {
			try {
				conn = DriverManager.getConnection(CONNECTIONSTRING);
			} catch (SQLException e) {
				throw new NoDatabaseFoundException("Database connection error!");
			}
		}

		return conn;
	}

	/**
	 * Copy one ITrainer object to another ITrainer object
	 * 
	 * @param fromTrainer ITrainer object to copy from
	 * @return the copied ITrainer object
	 */
	private Trainer copyTrainer(ITrainer fromTrainer) {

		Trainer resultTrainer = new Trainer();

		resultTrainer.setId(fromTrainer.getId());
		resultTrainer.setAge(fromTrainer.getAge());
		resultTrainer.setName(fromTrainer.getName());
		resultTrainer.setExperience(fromTrainer.getExperience());

		return resultTrainer;
	}

	/**
	 * Closes Statement and ResultSet objects
	 * 
	 * @param stmt Statement object
	 * @param rs   ResultSet object
	 */
	private void closer(Statement stmt, ResultSet rs) {

		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}

	/**
	 * Closes Statemnt, ResultSet and PreparedStatement objects
	 * 
	 * @param stmt Statement object
	 * @param rs   ResultSet object
	 * @param ps   PreparedStatedment object
	 */
	private void closer(Statement stmt, ResultSet rs, PreparedStatement ps) {

		try {
			stmt.close();
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Convert a SQL Trainer to Java Trainer
	 * 
	 * @param rs      ResultSet object
	 * @param trainer ITrainer object
	 */
	private void convertSqlToTrainer(ResultSet rs, Trainer trainer) {

		try {
			trainer.setId(rs.getInt("id"));
			trainer.setAge(rs.getInt("age"));
			trainer.setName(rs.getString("name"));
			trainer.setExperience(rs.getInt("experience"));
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}
}
