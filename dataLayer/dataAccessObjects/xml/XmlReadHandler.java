package dataLayer.dataAccessObjects.xml;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import businessObjects.ITrainer;
import dataLayer.businessObjects.Trainer;

public class XmlReadHandler extends DefaultHandler {
	boolean bname;
	boolean bage;
	boolean bexperience;

	List<ITrainer> trainerList;

	public XmlReadHandler() {
		bname = false;
		bage = false;
		bexperience = false;

		trainerList = new ArrayList<ITrainer>();
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		System.out.println("hi");
		if (qName.equalsIgnoreCase("Trainer")) {
			Trainer trainer = new Trainer();

			trainer.setId(Integer.parseInt(attributes.getValue("id")));
			trainerList.add(trainer);
		} else if (qName.equalsIgnoreCase("name")) {
			bname = true;
		} else if (qName.equalsIgnoreCase("age")) {
			bage = true;
		} else if (qName.equalsIgnoreCase("experience")) {
			bexperience = true;
		}
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		ITrainer trainer = trainerList.get(trainerList.size()-1);
		if (bname) {
			trainer.setName(new String(ch, start, length));
			bname = false;
		}
		if (bage) {
			trainer.setAge(Integer.parseInt(new String(ch, start, length)));
			bage = false;
		}
		if (bexperience) {
			trainer.setExperience(Integer.parseInt(new String(ch, start, length)));
			bexperience = false;
		}
	}
	
	public List<ITrainer> getAllTrainer() {
		return trainerList;
	}
}
