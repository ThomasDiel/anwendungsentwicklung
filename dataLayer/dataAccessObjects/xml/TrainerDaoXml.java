package dataLayer.dataAccessObjects.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import businessObjects.ITrainer;
import dataLayer.businessObjects.Trainer;
import dataLayer.dataAccessObjects.ITrainerDao;
import exceptions.NoDatabaseFoundException;
import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;
import exceptions.NoTrainerFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TrainerDaoXml implements ITrainerDao {
	private final String NO_TRAINER_FOUND = "No Trainer found exception!";

	Comparator<ITrainer> trainerIdComperator = new Comparator<ITrainer>() {

		@Override
		public int compare(ITrainer o1, ITrainer o2) {
			return o1.getId() - o2.getId();
		}

	};

	@Override
	public ITrainer create() throws NoDatabaseFoundException {
		System.out.println("da");
		Comparator<ITrainer> comp = new Comparator<ITrainer>() {

			@Override
			public int compare(ITrainer o1, ITrainer o2) {
				return o1.getId() - o2.getId();
			}

		};
		ITrainer maxIdTrainer = Collections.max(readXml(), comp);
		Trainer newTrainer = new Trainer();
		newTrainer.setId(maxIdTrainer.getId() + 1);
		System.out.println(newTrainer.getId());
		return newTrainer;
	}

	@Override
	public void delete(ITrainer trainer) throws NoDatabaseFoundException {
		List<ITrainer> trainerList = readXml();
		for (ITrainer current : trainerList) {
			if (current.getId() == trainer.getId()) {
				trainerList.remove(current);
				break;
			}
		}
		writeXML(trainerList);
	}

	@Override
	public ITrainer first() throws NoDatabaseFoundException {
		ITrainer minIdTrainer = Collections.min(readXml(), trainerIdComperator);
		return minIdTrainer;
	}

	@Override
	public ITrainer last() throws NoDatabaseFoundException {
		ITrainer maxIdTrainer = Collections.max(readXml(), trainerIdComperator);
		return maxIdTrainer;
	}

	@Override
	public ITrainer next(ITrainer trainer)
			throws NoDatabaseFoundException, NoNextTrainerFoundException, NoTrainerFoundException {
		List<ITrainer> trainerList = readXml();
		boolean found = false;
		for (ITrainer current : trainerList) {
			if (found) {
				return current;
			}
			if (current.getId() == trainer.getId()) {
				found = true;
			}
		}
		if (found) {
			throw new NoNextTrainerFoundException("No next Trainer found!");
		} else {
			throw new NoTrainerFoundException(NO_TRAINER_FOUND);
		}
	}

	@Override
	public ITrainer previous(ITrainer trainer)
			throws NoDatabaseFoundException, NoPreviousTrainerFoundException, NoTrainerFoundException {
		List<ITrainer> trainerList = readXml();
		ITrainer prev = null;
		for (ITrainer current : trainerList) {
			if (current.getId() == trainer.getId()) {
				if (prev == null) {
					throw new NoPreviousTrainerFoundException("No previous Trainer found!");
				} else {
					return prev;
				}
			}
			prev = current;
		}
		throw new NoTrainerFoundException(NO_TRAINER_FOUND);
	}

	@Override
	public void save(ITrainer trainer) throws NoDatabaseFoundException {
		List<ITrainer> tempList = readXml();
		boolean found = false;
		for (ITrainer element : tempList) {
			if (element.getId() == trainer.getId()) {
				element.setAge(trainer.getAge());
				element.setExperience(trainer.getExperience());
				element.setName(trainer.getName());
				found = true;
			}
		}
		if (!found) {
			tempList.add(trainer);
		}
		writeXML(tempList);
	}

	@Override
	public List<ITrainer> select() throws NoDatabaseFoundException {
		List<ITrainer> trainerList = readXml();
		return trainerList;
	}

	@Override
	public ITrainer select(int id) throws NoDatabaseFoundException, NoTrainerFoundException {
		List<ITrainer> trainerList = readXml();
		for (ITrainer current : trainerList) {
			if (current.getId() == id) {
				return current;
			}
		}
		throw new NoTrainerFoundException(NO_TRAINER_FOUND);
	}

	private List<ITrainer> readXml() throws NoDatabaseFoundException {
		List<ITrainer> result = new ArrayList<ITrainer>();

		File inputFile = new File("trainer.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList trainers = doc.getElementsByTagName("Trainer");

			for (int i = 0; i < trainers.getLength(); i++) {
				Trainer newTrainer = new Trainer();
				newTrainer.setId(Integer.parseInt(trainers.item(i).getAttributes().getNamedItem("id").getNodeValue()));
				if (trainers.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element trainer = (Element) trainers.item(i);
					newTrainer.setName(trainer.getElementsByTagName("name").item(0).getTextContent());
					newTrainer.setAge(Integer.parseInt(trainer.getElementsByTagName("age").item(0).getTextContent()));
					newTrainer.setExperience(
							Integer.parseInt(trainer.getElementsByTagName("experience").item(0).getTextContent()));
					result.add(newTrainer);
				}

			}
		} catch (ParserConfigurationException | SAXException | IOException e1) {
			System.err.println("");
		}
		return result;
	}

	private void writeXML(List<ITrainer> trainerList) {
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();

			// root element
			Element root = document.createElement("Trainers");
			document.appendChild(root);

			trainerList.sort(trainerIdComperator);

			// employee element
			for (ITrainer current : trainerList) {
				Element trainerXml = document.createElement("Trainer");
				root.appendChild(trainerXml);

				// set an attribute to staff element
				Attr attr = document.createAttribute("id");
				attr.setValue(String.valueOf(current.getId()));
				trainerXml.setAttributeNode(attr);

				// name element
				Element name = document.createElement("name");
				name.appendChild(document.createTextNode(current.getName()));
				trainerXml.appendChild(name);

				// age element
				Element age = document.createElement("age");
				age.appendChild(document.createTextNode(String.valueOf(current.getAge())));
				trainerXml.appendChild(age);

				// experience element
				Element experience = document.createElement("experience");
				experience.appendChild(document.createTextNode(String.valueOf(current.getExperience())));
				trainerXml.appendChild(experience);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File("trainer.xml"));

			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}

}
