package dataLayer;

import dataLayer.dataAccessObjects.sqlite.DataLayerSqlite;
import dataLayer.dataAccessObjects.xml.DataLayerXml;
import dataLayer.settings.PersistenceSettings;
import dataLayer.settings.SettingsManager;

/**
 * 
 */
public class DataLayerManager {
	private static DataLayerManager instance = null;
	private IDataLayer dataLayer;

	private DataLayerManager() {
		PersistenceSettings setting = SettingsManager.getInstance().getPersistenceSettings();
		switch (setting.getType()) {
		case "xml":
			dataLayer = new DataLayerXml();
			break;
		case "sqlite":

	         try {
				Class.forName("org.sqlite.JDBC");
			} catch (ClassNotFoundException e) {
				System.err.println("Class not found");
			}
			dataLayer = new DataLayerSqlite();
			break;
		default:
			dataLayer = null;
			System.err.println("Unknown mode! Please restart program after changing mode");
		}
	}

	public static DataLayerManager getInstance() {
		if (instance == null) {
			instance = new DataLayerManager();
		}
		return instance;
	}

	public IDataLayer getDataLayer() {
		return dataLayer;
	}
}
