package dataLayer.settings;

/**
 * PersistenceSettings is a storage class to remember the type of data access
 * operation to be used.
 * @author Ricky Rusli
 * 
 */
public class PersistenceSettings {
	private String type;

	/**
	 * 
	 * @param type run type as string
	 */
	public PersistenceSettings(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
