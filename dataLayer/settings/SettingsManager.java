package dataLayer.settings;

import java.io.*;

/**
 * SettingsManager is a singleton class managing the settings by reading a "Mode.cfg"
 * file and creating the equivalent PersistenceSetting for the file entry.
 * @author Ricky Rusli
 */
public class SettingsManager {
	private static SettingsManager instance = null;
	private PersistenceSettings persistenceSettings;
	private final String SETTINGS_PATH = "Mode.cfg";

	private SettingsManager() {
		try {
			persistenceSettings = readPersistenceSettings();
		} catch (IOException e) {
			System.err.println("Mode.cfg could not be Read!!");
			persistenceSettings = null;
		}
	}

	public static SettingsManager getInstance() {
		if(instance == null) {
			instance = new SettingsManager();
		}
		return instance;
	}

	public PersistenceSettings getPersistenceSettings() {
		return persistenceSettings;
	}

	/**
	 * Function to read Mode.cfg file and creates equivalent PersistenceSetting object
	 * @return object of PersistenceSetting containing the mode information from the file
	 * @throws IOException if error occures while opening and reading the Mode.cfg file
	 */
	private PersistenceSettings readPersistenceSettings() throws IOException {
		FileInputStream file = new FileInputStream(SETTINGS_PATH);
		BufferedReader reader = new BufferedReader(new InputStreamReader(file));
		String mode = reader.readLine();
		reader.close();
		return new PersistenceSettings(mode);
	}
}
