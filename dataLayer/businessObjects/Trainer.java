package dataLayer.businessObjects;

import businessObjects.ITrainer;

public class Trainer implements ITrainer{
	private int id;
	private String name;
	private int age;
	private int experience;
	
	public Trainer() {
		id = 0;
		name = "";
		age = 0;
		experience = 0;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int getExperience() {
		return experience;
	}

	@Override
	public void setExperience(int experience) {
		this.experience = experience;
	}

	@Override
	public String toString() {
		return "Trainer [id=" + id + ", name=" + name + ", age=" + age + ", experience=" + experience + "]";
	}
}
